<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('map');
});

Route::get('/map', function () {
    return view('map');
});

Route::get('/sert', function () {
    return Response::make(print_r(openssl_get_cert_locations(), true));
});


Route::get('infoMessageTemplate', function () {
    return view('infoMessage');
});
