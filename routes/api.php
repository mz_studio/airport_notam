<?php

Route::middleware('throttle:60,1')->group(function () {
    Route::get('notam/{notam}', 'Api\NotamController');

    Route::get('weather/{notam}', 'Api\WeatherController');
});
