<?php

namespace App\Providers;

use App\Contracts\IAirportDataProvider;
use App\DataProviders\RockerRoutHttpProvider;
use App\ExternalAPI\Contracts\ITokenStorage;
use App\ExternalAPI\RocketRoute\RocketRouteApi;
use App\ExternalAPI\Storages\CacheTokenStorage;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IAirportDataProvider::class, function ($app) {
            return new RockerRoutHttpProvider($app->make(RocketRouteApi::class));
        });

        $this->app->singleton(ITokenStorage::class, function ($app) {
            return new CacheTokenStorage();
        });

        $this->app->bind(RocketRouteApi::class, function ($app) {
            return new RocketRouteApi(
                env('ROCKET_ROUT_LOGIN', ''),
                env('ROCKET_ROUT_PASSWORD', ''),
                env('ROCKET_ROUT_SECRET_KEY', ''),
                env('ROCKET_ROUT_HOST', ''),
                $app->make(Client::class, ['config' => [
                    'curl' => [
                        CURLOPT_SSL_VERIFYPEER => false
                    ]
                ]]),
                $app->make(ITokenStorage::class)
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
