<?php

namespace App\Contracts\DataObjects;

/**
 * Interface IWeatherObj
 *
 * @property string $itemE
 *
 * @property array $coordinates - in Google format [lat => -34.397, lng => 150.644]
 *
 */
interface INotamObj
{

    /**
     * @param array $dataArray
     * @return INotamObj
     */
    public function loadData(array $dataArray): self;

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * return JSON
     * @return mixed
     */
    public function __toString(): string;
}
