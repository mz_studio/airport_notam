<?php

namespace App\Contracts\DataObjects;

/**
 * Interface IWeatherObj
 *
 * @property string $dateTime
 * @property string $wind
 * @property string $speed
 * @property string $visibility
 * @property string[] $clouds
 * @property string $temperature
 * @property string $relative_humidity
 *
 */
interface IWeatherObj
{

    /**
     * @param array $dataArray
     * @return IWeatherObj
     */
    public function loadData(array $dataArray): self;

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * return JSON
     * @return mixed
     */
    public function __toString(): string;
}
