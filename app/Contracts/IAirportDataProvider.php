<?php


namespace App\Contracts;

use App\Contracts\DataObjects\INotamObj;
use App\Contracts\DataObjects\IWeatherObj;

interface IAirportDataProvider
{
    /**
     * returns the weather data, available for airport
     * @param string $icao
     * @return IWeatherObj
     */
    public function getWeatherData(string $icao): IWeatherObj;

    /**
     * returns the array of NOTAMs which available for airport
     * only ItemE and ItemQ values
     * @param string $icao
     * @return INotamObj[]
     */
    public function getNotamData(string $icao): array;
}
