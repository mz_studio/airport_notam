<?php


namespace App\ExternalAPI\Contracts;

interface ITokenStorage
{
    /**
     * @param string $name
     * @return string|null
     */
    public function getToken(string $name):? string;

    /**
     * @param string $name
     * @param string $value
     */
    public function setToken(string $name, string $value): void;

    /**
     * @param string $name
     */
    public function setStorageName(string $name):void;
    /**
     * @return string
     */
    public function getStorageName(): string ;
}
