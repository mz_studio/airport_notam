<?php


namespace App\ExternalAPI\Contracts;


use GuzzleHttp\Psr7\Response;

interface IExternalApiResponse
{

    /**
     * Load data which external API returned
     * @param array $data
     * @return mixed
     */
    public function loadData(array $data): void;

    /**
     * Load response which external API returned
     * @param Response $response
     * @return mixed
     */
    public function loadPSRResponse(Response $response): void;


    /**
     * return PSR7 response or null
     * @return Response | null
     */
    public function getPSRResponse();

    /**
     * Was array success or not
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * return error message
     * @return string
     */
    public function getMessage(): string;

    /**
     * return array of fields which are in data section
     * @return array
     */
    public function getResponseParamsArray(): array;

    /**
     * @param bool $isSuccess
     * @return mixed
     */
    public function setIsSuccess(bool $isSuccess): void;

    /**
     * @param string $message
     * @return mixed
     */
    public function setMessage(string $message): void;

}
