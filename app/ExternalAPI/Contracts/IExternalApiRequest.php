<?php

namespace App\ExternalAPI\Contracts;

interface IExternalApiRequest
{
    /**
     * return array with request params
     * @return array
     */
    public function getBodyArray(): array;

    /**
     * set request params
     * @param array $dataArray
     * @return mixed
     */
    public function setBodyArray(array $dataArray): void;

    /**
     * return request method (post, get)
     * @return string
     */
    public function getMethod(): string;

    /**
     * return api endpoint
     * @return string
     */
    public function getEndPoint(): string;

    /**
     * return name of api method where concrete request is used
     * @return string
     */
    public function getApiMethodName(): string;
}
