<?php


namespace App\ExternalAPI\RocketRoute\responses;

use App\ExternalAPI\Contracts\IExternalApiRequest;
use App\ExternalAPI\Contracts\IExternalApiResponse;
use App\ExternalAPI\RocketRoute\Enums\ApiMethodsEnum;
use RuntimeException;

class ResponseFactory
{
    /**
     * @param IExternalApiRequest $request
     * @return IExternalApiResponse
     * @throws RuntimeException
     */
    public static function getResponse(IExternalApiRequest $request): IExternalApiResponse
    {
        switch ($request->getApiMethodName()) {
            case ApiMethodsEnum::LOGIN:
                return new LoginResp();
            case ApiMethodsEnum::GET_NOTAM:
                return new NotamResp();
            case ApiMethodsEnum::GET_WEATHER:
                return new WeatherResp();
        }
        throw new RuntimeException('Passed incorrect Request');
    }
}
