<?php

namespace App\ExternalAPI\RocketRoute\responses;

use App\ExternalAPI\Contracts\IExternalApiResponse;
use GuzzleHttp\Psr7\Response;

class BaseResponse implements IExternalApiResponse
{
    protected $isSuccess = false;
    protected $statusMessage = '';

    protected $originDataArray = [];
    /**
     * @var Response $psrResponse
     */
    protected $psrResponse;

    public function loadData(array $responseData): void
    {
        $this->originDataArray = $responseData;

        $this->isSuccess = $responseData['status']['success'] ?? false;
        $this->statusMessage = $responseData['status']['message'] ?? '';
    }

    /**
     * Was array success or not
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    /**
     * return error message
     * @return string
     */
    public function getMessage(): string
    {
        return $this->statusMessage;
    }

    /**
     * return array of fields which are in data section
     * @return array
     */
    public function getResponseParamsArray(): array
    {
        return $this->originDataArray['data'] ?? [];
    }

    /**
     * @param bool $isSuccess
     * @return mixed
     */
    public function setIsSuccess(bool $isSuccess): void
    {
        $this->isSuccess = $isSuccess;
    }

    /**
     * @param string $message
     * @return mixed
     */
    public function setMessage(string $message): void
    {
        $this->statusMessage = $message;
    }

    /**
     * Load response which external API returned
     * @param Response $response
     * @return mixed
     */
    public function loadPSRResponse(Response $response): void
    {
        $this->psrResponse = $response;
        $dataString = (string)$response->getBody();
        $dataArray = [];
        try {
            $dataArray = json_decode($dataString, true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            \Log::error($e);
        }

        $this->loadData($dataArray);
    }

    /**
     * return PSR7 response or null
     * @return Response | null
     */
    public function getPSRResponse(): ?Response
    {
        return $this->psrResponse;
    }
}
