<?php

namespace App\ExternalAPI\RocketRoute\responses;

class LoginResp extends BaseResponse
{

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->psrResponse->getHeader('Authorization')[0] ?? '';
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->getResponseParamsArray()['refresh_token'] ?? '';
    }
}
