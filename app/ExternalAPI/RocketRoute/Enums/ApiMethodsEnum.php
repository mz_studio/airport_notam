<?php


namespace App\ExternalAPI\RocketRoute\Enums;

/**
 * list of supported API methods
 */
class ApiMethodsEnum
{
    public const LOGIN = 'login';
    public const GET_NOTAM = 'getNotam';
    public const GET_WEATHER = 'getWeather';
}
