<?php

namespace App\ExternalAPI\RocketRoute\requests;

use App\ExternalAPI\Contracts\IExternalApiRequest;

class BaseRequest implements IExternalApiRequest
{

    protected $bodyArray = [];
    protected $method = 'get';
    protected $endpoint = '';
    protected $apiMethod ='';

    /**
     * return array with request params
     * @return array
     */
    public function getBodyArray(): array
    {
        return $this->bodyArray;
    }

    /**
     * set request params
     * @param array $dataArray
     * @return mixed
     */
    public function setBodyArray(array $dataArray): void
    {
        $this->bodyArray = $dataArray;
    }

    /**
     * return request method (post, get)
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * return api endpoint
     * @return string
     */
    public function getEndPoint(): string
    {
        return $this->endpoint;
    }

    /**
     * return name of api method where concrete request is used
     * @return string
     */
    public function getApiMethodName(): string
    {
        return $this->apiMethod;
    }
}
