<?php


namespace App\ExternalAPI\RocketRoute\requests;

use App\ExternalAPI\RocketRoute\Enums\ApiMethodsEnum;

class LoginRequest extends BaseRequest
{
    protected $method = 'post';
    protected $endpoint = '/api/login';

    protected $apiMethod = ApiMethodsEnum::LOGIN;
}
