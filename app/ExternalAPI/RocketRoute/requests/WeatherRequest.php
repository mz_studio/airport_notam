<?php


namespace App\ExternalAPI\RocketRoute\requests;

use App\ExternalAPI\Contracts\IRequireLoginInterface;
use App\ExternalAPI\RocketRoute\Enums\ApiMethodsEnum;

class WeatherRequest extends BaseRequest implements IRequireLoginInterface
{
    protected $method = 'post';
    protected $endpoint = '/api/weather/get';

    protected $apiMethod = ApiMethodsEnum::GET_WEATHER;
}
