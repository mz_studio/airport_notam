<?php


namespace App\ExternalAPI\RocketRoute\requests;

use App\ExternalAPI\Contracts\IRequireLoginInterface;
use App\ExternalAPI\RocketRoute\Enums\ApiMethodsEnum;

class NotamRequest extends BaseRequest implements IRequireLoginInterface
{
    protected $method = 'post';
    protected $endpoint = '/api/data/notam';

    protected $apiMethod = ApiMethodsEnum::GET_NOTAM;
}
