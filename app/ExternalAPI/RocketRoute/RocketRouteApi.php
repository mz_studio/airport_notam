<?php

namespace App\ExternalAPI\RocketRoute;

use App\ExternalAPI\Contracts\IExternalApiRequest;
use App\ExternalAPI\Contracts\IExternalApiResponse;
use App\ExternalAPI\Contracts\IRequireLoginInterface;
use App\ExternalAPI\Contracts\ITokenStorage;
use App\ExternalAPI\RocketRoute\requests\LoginRequest;
use App\ExternalAPI\RocketRoute\requests\NotamRequest;
use App\ExternalAPI\RocketRoute\requests\WeatherRequest;
use App\ExternalAPI\RocketRoute\responses\LoginResp as LoginRespAlias;
use App\ExternalAPI\RocketRoute\responses\NotamResp;
use App\ExternalAPI\RocketRoute\responses\ResponseFactory;
use App\ExternalAPI\RocketRoute\responses\WeatherResp;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use RuntimeException;

class RocketRouteApi
{
    private $email;
    private $password;
    private $appKey;
    private $host;

    /**
     * @var ITokenStorage
     */
    private $tokenStorage;

    /**
     * @var Client
     */
    private $httpClient;

    private $accessToken;
    private $refreshToken;


    /**
     * RocketRouteApi constructor.
     * @param string $email
     * @param string $password
     * @param string $appKey
     * @param string $host
     * @param Client $httpClient
     * @param ITokenStorage $tokenStorage
     * @throws RuntimeException
     */
    public function __construct(
        string $email,
        string $password,
        string $appKey,
        string $host,
        Client $httpClient,
        ITokenStorage $tokenStorage
    ) {
        $this->email = $email;
        $this->password = $password;
        $this->appKey = $appKey;
        $this->host = $host;

        $this->httpClient = $httpClient;
        $this->tokenStorage = $tokenStorage;

        if (!$this->login()) {
            throw new RuntimeException('Login error');
        }
    }

    /**
     * @return LoginRespAlias
     */
    public function getlogin(): LoginRespAlias
    {
        $request = new LoginRequest();
        $request->setBodyArray([
            'email' => $this->email,
            'password' => $this->password,
            'app_key' => $this->appKey,
        ]);
        return $this->callToRocketRout($request);
    }

    /**
     * @return LoginRespAlias
     */
    public function getLoginWithRefreshToken(): LoginRespAlias
    {
        $request = new LoginRequest();
        $request->setBodyArray([
            'refresh_token' => $this->refreshToken,
            'app_key' => $this->appKey,
        ]);
        return $this->callToRocketRout($request, false);
    }

    /**
     * @param string $icao
     * @return NotamResp
     */
    public function getNotam(string $icao): NotamResp
    {
        $request = new NotamRequest();
        $request->setBodyArray(['airport' => $icao]);
        return $this->callToRocketRout($request);
    }

    /**
     * @param string $icao
     * @return WeatherResp
     */
    public function getWeather(string $icao): WeatherResp
    {
        $request = new WeatherRequest();
        $request->setBodyArray(['airport' => $icao]);
        return $this->callToRocketRout($request);
    }

    /**
     * @param IExternalApiRequest $request
     * @return IExternalApiResponse | LoginRespAlias | NotamResp | WeatherResp
     */
    private function callToRocketRout(IExternalApiRequest $request, bool $retryAllowed = true): IExternalApiResponse
    {
        $rocketResponse = ResponseFactory::getResponse($request);
        $psrRequest = $this->preparePsrRequest($request);
        try {
            $psrResponse = $this->httpClient->send($psrRequest);
            $rocketResponse->loadPSRResponse($psrResponse);
        } catch (ClientException $e) {

            //try to get login with refresh token
            if ($e->getResponse()->getStatusCode() === 403 && $retryAllowed) {
                $this->login(true);
                return $this->callToRocketRout($request, false);
            }

            $rocketResponse->setMessage($e->getMessage());
            \Log::error($e);
        } catch (ServerException $e) {
            $rocketResponse->setMessage($e->getMessage());
            \Log::error($e);
        } catch (RequestException $e) {
            $rocketResponse->setMessage($e->getMessage());
            \Log::error($e);
        }
        return  $rocketResponse;
    }

    /**
     * @param IExternalApiRequest $request
     * @return Request
     */
    private function preparePsrRequest(IExternalApiRequest $request): Request
    {
        $headers = [
            'Content-Type' => 'application/json',
            'X-API-Version' => '1.0'
        ];
        if ($request instanceof IRequireLoginInterface) {
            $headers['Authorization'] = $this->accessToken;
        }

        $body = $request->getBodyArray() !== [] ? json_encode($request->getBodyArray(), JSON_THROW_ON_ERROR) : null;

        return new Request(
            $request->getMethod(),
            $this->host . $request->getEndPoint(),
            $headers,
            $body
        );
    }

    /**
     * retrieve and save auth tokens
     * @param bool $refresh
     * @return bool
     */
    private function login(bool $refresh = false): bool
    {
        $this->accessToken = $this->tokenStorage->getToken('access_token');
        $this->refreshToken = $this->tokenStorage->getToken('refresh_token');

        if (!$this->accessToken || !$this->refreshToken) {
            $response = $this->getlogin();
            return $this->saveTokens($response);
        }

        if ($refresh) {
            //$response = $this->getlogin();
            $response = $this->getLoginWithRefreshToken();
            return $this->saveTokens($response);
        }

        return true;
    }

    /**
     * @param IExternalApiResponse $response
     * @return bool
     */
    private function saveTokens(IExternalApiResponse $response): bool
    {
        if ($response->isSuccess()) {
            $this->accessToken = $response->getAccessToken();
            $this->refreshToken = $response->getRefreshToken();
            $this->tokenStorage->setToken('access_token', $response->getAccessToken());
            $this->tokenStorage->setToken('refresh_token', $response->getRefreshToken());
            return true;
        }
        return false;
    }
}
