<?php


namespace App\ExternalAPI\Storages;

use App\ExternalAPI\Contracts\ITokenStorage;
use Cache;
use DateInterval;

class CacheTokenStorage implements ITokenStorage
{

    private $storageName = 'commonTokenStorage:';
    /**
     * @param string $name
     * @return string|null
     */
    public function getToken(string $name): ?string
    {
        return Cache::get($this->storageName . $name);
    }

    /**
     * @param string $name
     * @param string $value
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function setToken(string $name, string $value): void
    {
        Cache::set($this->storageName . $name, $value, DateInterval::createFromDateString('2 hours'));
    }

    /**
     * @param string $name
     */
    public function setStorageName(string $name): void
    {
        $this->storageName = $name;
    }

    /**
     * @return string
     */
    public function getStorageName(): string
    {
        return $this->storageName;
    }
}
