<?php

namespace App\DataProviders;

use App\Contracts\DataObjects\INotamObj;
use App\Contracts\DataObjects\IWeatherObj;
use App\Contracts\IAirportDataProvider;
use App\DataProviders\DataObjects\Notam;
use App\DataProviders\DataObjects\Weather;
use App\ExternalAPI\RocketRoute\RocketRouteApi;

class RockerRoutHttpProvider implements IAirportDataProvider
{
    private $providerAPI;

    public function __construct(RocketRouteApi $providerAPI)
    {
        $this->providerAPI = $providerAPI;
    }

    /**
     * returns the weather data, available for airport
     * @param string $icao
     * @return IWeatherObj
     */
    public function getWeatherData(string $icao): IWeatherObj
    {
        $weatherData = $this->providerAPI->getWeather($icao);
        $weatherObj = new Weather();
        $weatherObj->loadData($weatherData->getResponseParamsArray()[0]['weather']['metar_decoded'] ?? []);
        return $weatherObj;
    }

    /**
     * returns the array of NOTAMs which available for airport
     * only ItemE and ItemQ values
     * @param string $icao
     * @return INotamObj[]
     */
    public function getNotamData(string $icao): array
    {
        $notamData = $this->providerAPI->getNotam($icao);
        $notams = $notamData->getResponseParamsArray()[0]['notams'] ?? [];
        $notamsArray = [];
        foreach ($notams as $notam) {
            $notamsArray[] = (new Notam())->loadData($notam);
        }
        return $notamsArray;
    }
}
