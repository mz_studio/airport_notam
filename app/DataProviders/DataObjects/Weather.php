<?php

namespace App\DataProviders\DataObjects;

use App\Contracts\DataObjects\IWeatherObj;

class Weather implements IWeatherObj
{
    public $datetime = '';
    public $wind = '';
    public $speed = '';
    public $visibility = '';
    public $clouds = [];
    public $temperature = '';
    public $relative_humidity = '';
    public $dewpoint = '';
    public $qnh_sea_level_pressure = '';

    /**
     * @param array $dataArray
     * @return IWeatherObj
     */
    public function loadData(array $dataArray): IWeatherObj
    {
        foreach ($dataArray as $key => $value) {
            $property = mb_strtolower($key);
            if (property_exists($this, $property)) {
                $this->$property = $value['display'] ?? (is_array($value) ? $this->getAllDisplayData($value) : $value);
            }
        }
        return $this;
    }

    private function getAllDisplayData(array $paramsArray): array
    {
        return array_reduce($paramsArray, function ($result, $item) {
            $result[] = $item['display'] ?? '';
            return $result;
        }, $resultArray = []);
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_filter(
            (array)$this,
            function ($value) {
                return (bool)$value;
            }
        );
    }

    /**
     * return JSON string
     * @return mixed
     */
    public function __toString(): string
    {
        return (string)json_encode($this->toArray(), JSON_THROW_ON_ERROR, 512);
    }
}
