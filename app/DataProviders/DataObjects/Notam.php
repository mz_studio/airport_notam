<?php

namespace App\DataProviders\DataObjects;

use App\Contracts\DataObjects\INotamObj;

class Notam implements INotamObj
{
    public $itemE = '';
    public $coordinates = [];

    /**
     * @param array $dataArray
     * @return INotamObj
     */
    public function loadData(array $dataArray): INotamObj
    {
        $this->itemE = $dataArray['ItemE'] ?? '';
        $this->coordinates = $this->transformItemQToGooleCoordinates($dataArray['ItemQ'] ?? '');
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return (array) $this;
    }

    /**
     * return JSON string
     * @return mixed
     */
    public function __toString(): string
    {
        return (string)json_encode($this->toArray(), JSON_THROW_ON_ERROR, 512);
    }

    /**
     * @param string $itemQ
     * @return array
     */
    private function transformItemQToGooleCoordinates(string $itemQ): array
    {
        $dataArray = explode('/', $itemQ);
        $coordinates = end($dataArray);
        if (!$coordinates) {
            return [];
        }
        preg_match('/^([\d]{2})([\d]{2})([N,S])([\d]{3})([\d]{2})([W,E])$/', $coordinates, $substringArray);
        if (count($substringArray) !== 7) {
            return [];
        }
        return [
            'lat' => (float)(($substringArray[3] === 'S' ? '-' : '' ) . $substringArray[1] . '.' . $this->minutesToDecimal((int)$substringArray[2])),
            'lng' => (float)(($substringArray[6] === 'W' ? '-' : '' ) . $substringArray[4] . '.' . $this->minutesToDecimal((int)$substringArray[5]))
        ];
    }

    /**
     * @param int $minutes
     * @return mixed
     */
    private function minutesToDecimal(int $minutes)
    {
        $fractionalPart = (string)round($minutes / 60, 5);
        return str_replace('0.', '', $fractionalPart);
    }
}
