<?php

namespace App\Http\Controllers\Api;

use App\Contracts\IAirportDataProvider;
use Illuminate\Routing\Controller;

class NotamController extends Controller
{
    private $dataProvider;

    public function __construct(IAirportDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param string $notam
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke($notam = '')
    {
        $notamData = $this->dataProvider->getNotamData($notam);
        return \Response::json($notamData);
    }
}
