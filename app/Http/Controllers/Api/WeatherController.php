<?php

namespace App\Http\Controllers\Api;

use App\Contracts\IAirportDataProvider;
use Illuminate\Routing\Controller;

class WeatherController extends Controller
{
    private $dataProvider;

    public function __construct(IAirportDataProvider $dataProvider)
    {
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param string $notam
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke($notam = '')
    {
        $weatherData = $this->dataProvider->getWeatherData($notam);
        return \Response::json($weatherData->toArray());
    }
}
