export class HttpClient {

    constructor(client, logger) {
        this.client = client;
        this.logger = logger;
    }

    /**
     * Create http request
     * method - GET, POST
     * URL - service url
     * data - object with params
     * Return deferred object
     * */
    createRequest(method, url, params = {}, data = null, typeContent = 'json') {
        this.logger.log('Start ' + method + 'request to ' + url);
        this.logger.log(data);
        this.logger.log(params);

        return this.client({
            method: method,
            url: url,
            data: data,
            params: params,
            responseType: typeContent
        });
    }
}
