export class Preloader {
    constructor() {
        this.id = 'preloader';

        this.preloader = document.getElementById(this.id);
    }

    show() {
        this.preloader.style.display = 'flex'
    };

    hide() {
        this.preloader.style.display = 'none'
    };
}
