export class MapFacade{
    constructor(infoMessageTemplate, startCoordinates){
        this.mapContainerId = 'map';

        this.tamplate = infoMessageTemplate;

        this.map = new google.maps.Map(document.getElementById(this.mapContainerId), {
            center: startCoordinates,
            zoom: 10
        });

        this.markerStorage = [];

        this.addMarker('RocketRoute', startCoordinates, '<h1>Hello!</h1>');
        this.showMarkers();
    }


    loadData(icao, notams, weather) {
        this.deleteMarkers();
        let lasCoordinates = {};
        for (var i = 0; i < notams.length; i++) {
            if (notams[i]['coordinates'] == null || Array.isArray(notams[i]['coordinates']) ) {
                continue;
            }
            let data = {
                icao: icao,
                notam: {itemE: notams[i]['itemE']},
                weather: weather
            };
            this.addMarker(icao, notams[i]['coordinates'], (!!data ? this.createInfoMessage(data) : ''));
            lasCoordinates = notams[i]['coordinates'];
        }
        this.map.setCenter(lasCoordinates);
    }

    showMarkers() {
        this.setMapOnAll(this.map);
    }

    // Sets the map on all markers in the array.
    setMapOnAll(map) {
        for (var i = 0; i < this.markerStorage.length; i++) {
            (this.markerStorage[i]).marker.setMap(map);
        }
    }

    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        if (this.markerStorage.length > 0) {
            this.setMapOnAll(null);
            this.markerStorage = [];
        }
    }

    addMarker(title, location, dataInfo = null) {
        let markerObj = {marker: null, infoWindow: null};

        markerObj.infowindow = new google.maps.InfoWindow({
            content: dataInfo
        });

        markerObj.marker = new google.maps.Marker({
            position: location,
            map: this.map,
            icon: window.document.config.markerIconUrl,
            title: title
        });

        markerObj.marker.addListener('click', function () {
            markerObj.infowindow.open(this.map, markerObj.marker);
        }.bind(this));
        this.markerStorage.push(markerObj);
    }

    createInfoMessage(data) {
        return Mustache.render(this.tamplate, data)
    }
}
