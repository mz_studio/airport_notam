export class RocketRoutDataProvider {

    constructor(apiUrl, httpClient) {
        this.apiUrl = apiUrl;
        this.httpClient = httpClient;

        this.getNotamEndPoint = '/api/notam';
        this.getWeatherEndPoint = '/api/weather';
        this.getAirportCoordinatesEndpoint = '/api/airport';
    };

    /**
     * return NOTAM for ICAO
     * */
    getNOTAM(ICAOCode) {
        return this.httpClient.createRequest(
            'get',
            this.apiUrl + this.getNotamEndPoint + '/' + ICAOCode
        )
    }

    /**
     * return Weather for ICAO
     * */
    getWeather(ICAOCode) {
        return this.httpClient.createRequest(
            'get',
            this.apiUrl + this.getWeatherEndPoint + '/' + ICAOCode
        )
    }

    getInfoMessageTemplate() {
        return this.httpClient.createRequest(
            'get',
            this.apiUrl + '/infoMessageTemplate',
            {},
            null,
            'text'
        )
    }
}
