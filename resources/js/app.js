import {Logger} from "./Logger";
import {RocketRoutDataProvider} from "./RocketRout/RocketRoutDataProvider";
import {HttpClient} from "./HttpClient";
import {MapFacade} from "./map/MapFacade";
import {Preloader} from "./Preloader";
import {Informer} from "./Informer";

require('./bootstrap');

window.document.config = {
    host: "http://airport_locator.test",
    markerIconUrl: "http://airport_locator.test/img/warning-icon-th1.png"
};

let logger = new Logger(true);
let httpClient = new HttpClient(axios, logger);
let rrDataProvider = new RocketRoutDataProvider(window.document.config.host, httpClient);
let preloader = new Preloader();
let informer = new Informer();

let mapFacade;

let airportInput = document.getElementById('rocket-input');
let airportButton = document.getElementById('rocket-button');

let inputValidator = function (icao) {
    return /^[a-zA-Z]{4}$/.test(icao);
};

/**
 * The Google Map is mandatory part of application. this function will be called after loading maps.googleapis.com
 * */
window.initMap = function () {
    rrDataProvider.getInfoMessageTemplate().then(function (response) {
        logger.log(response);

        mapFacade = new MapFacade(response.data, {lat: 49.832724, lng: 24.024550})
    }).catch(function (error) {
        informer.send('Unexpected error. Please, try later.');
        logger.log(error);
    });

    airportButton.onclick = async function () {
        let icao = airportInput.value;
        preloader.show();
        if (inputValidator(icao)) {
            try {
                let notams = await rrDataProvider.getNOTAM(icao);
                let weather = await rrDataProvider.getWeather(icao);
                logger.log(notams.data);
                logger.log(weather.data);

                if (notams.data.length === 0) {
                    informer.send('For airport ' + icao + 'information hasn\'t found.')
                }
                mapFacade.loadData(icao, notams.data, weather.data);
                mapFacade.showMarkers();

                //mapFacade.addMarker(icao, {lat: -36.397, lng: 152.644})

            } catch (e) {
                informer.send('Unexpected error. Please, try later.');
                logger.log('Load data error');
                logger.log(e);
            }

        } else {
            informer.send('The ICAO code should be 4 letters long. Only latin letters are allowed.');
        }
        preloader.hide();
    };
};
