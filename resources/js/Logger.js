export class Logger {
    constructor(isTest) {
        this.isTest = isTest;
    }

    /**
     * Log something in console
     * */
    log(message){
        if (this.isTest === true) {
            console.log(message)
        }
    }
}
