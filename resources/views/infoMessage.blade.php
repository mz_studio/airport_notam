<div id="content">
    <div id="siteNotice">
    </div>
    <h1 id="firstHeading" class="firstHeading">Airport - @{{ icao }}</h1>
    <div id="bodyContent">
        @{{notam.itemE}}
    </div>

    <h1 id="firstHeading" class="firstHeading">Weather</h1>
    <div id="bodyContent">
        @{{#weather.datetime}}
            <b>Date:</b> @{{weather.datetime}}
            <br/>
        @{{/weather.datetime}}
        @{{#weather.clouds}}
        <b>Clouds: </b>
            @{{.}}
        <br/>
        @{{/weather.clouds}}
        @{{#weather.dewpoint}}
        <b>Dewpoint: </b> @{{weather.dewpoint}}
        <br/>
        @{{/weather.dewpoint}}
        @{{#weather.wind}}
        <b>Wind: </b> @{{weather.wind}}
        <br/>
        @{{/weather.wind}}
    </div>
</div>
