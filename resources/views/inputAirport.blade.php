<div class="input-airport">
    <label for="rocket-input">ICAO:</label>
    <input placeholder="Type code..." id="rocket-input" class="rocketInput" type="text" maxlength="4" pattern="^[a-zA-Z]{4}$">
    <button id="rocket-button">Find</button>
</div>

