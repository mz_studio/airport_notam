<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">

    <link rel="stylesheet" href="/css/app.css">
</head>
<body>

<div id="preloader"  class="container" style="display: none">
    <div class="item-1"><div></div></div>
    <div class="item-2"><div></div></div>
    <div class="item-3"><div></div></div>
    <div class="item-4"><div></div></div>
    <div class="item-5"><div></div></div>
    <div class="item-6"><div></div></div>
    <div class="item-7"><div></div></div>
    <div class="item-8"><div></div></div>
    <div class="item-9"><div></div></div>
</div>

    <div id="map"></div>

    @include('inputAirport')


<script src="/js/app.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY', '') }}&callback=initMap"
        async defer></script>
</body>
</html>
